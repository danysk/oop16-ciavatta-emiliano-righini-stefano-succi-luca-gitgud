package org.gitgud.application.toolbar;

interface ToolbarView {

    void attachController(ToolbarController ctrl);

}

package org.gitgud.application.toolbar;

import org.gitgud.utils.Utils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * An implementation of a toolbar view.
 */
public class ToolbarFxmlController implements ToolbarView {

    private ToolbarController ctrl;

    @Override
    public void attachController(final ToolbarController ctrl) {
        this.ctrl = ctrl;
    }

    @FXML // //
    void onBranchAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doBranchAction());
    }

    @FXML // //
    void onDiffAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doDiffAction());
    }

    @FXML // //
    void onLogAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doLogAction());
    }

    @FXML // //
    void onMergeModeAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doMergeMode());
    }

    @FXML // //
    void onPullAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doPullAction());
    }

    @FXML // //
    void onPushAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doPushAction());
    }

    @FXML // //
    void onRebaseAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doRebaseAction());
    }

    @FXML // //
    void onRemoteAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doRemoteAction());
    }

    @FXML // //
    void onRepositoryAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doRepositoryAction());
    }

    @FXML // //
    void onSettingsAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doSettingsAction());
    }

    @FXML // //
    void onStageAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doStageAction());
    }

    @FXML // //
    void onStashAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doStashAction());
    }

    @FXML // //
    void onTagAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doTagAction());
    }

    @FXML // //
    void onUserAction(final ActionEvent event) {
        Utils.doHardWork(() -> ctrl.doUserAction());
    }

}

package org.gitgud.application.node;

/**
 * Represent a generic box.
 */
public interface Box extends Panel {

    /**
     * Close the box.
     */
    void closeBox();

}
